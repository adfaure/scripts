#!/bin/bash

# This scrit can be use to install
# a package or an environment into
# every nodes of a job reserved by
# oar. each node must have nix installed.

set -exu

# Set the function that create the master
function run_master() {
 MASTER=$1
 ENVNAME=$2
 CLOSURE=$3

 # If the closure file does not exits, we create it.
 if [ ! -f ${CLOSURE} ]; then
   echo "Closure not created... Creating"
   oarsh ${MASTER} "~/scripts/build_nix_closure.sh -e ${ENVNAME} -f $PKGS -o \
     ${CLOSURE}"
 else
  echo "Closure file exists"
  oarsh ${MASTER} "cat ${CLOSURE} |bunzip2 |nix-store \
--import"
 fi

 oarsh ${MASTER} "nix-env -iA ${ENVNAME} -f $PKGS"

}

JOBID=${OAR_JOB_ID}
ENVNAME=$1
CLOSURE=$2

if [ -z ${CLOSURE+x} ]; then
  echo "closure unset, creating temporary path"
  CLOSURE="/home/afaure/$ENVNAME.bz2"
fi

# Todo
PKGS="/home/afaure/myPkgs"

# We use oarprint to get the list
# of node allocated for the current job
# https://superuser.com/a/121628
NODES=(`oarprint host -P host -F '%'`)
# The three following lines extract
# the first element form the array
set -- $NODES
MASTER=$1
echo "master is $MASTER"

# Launch the master
# MPID is a result var
echo Select a master:$MASTER
run_master ${MASTER} ${ENVNAME} ${CLOSURE}

# Get the list of slaves
SLAVES="${NODES[@]:1}" # removed the 1st parameter
echo "slaves:$SLAVES"

# https://stackoverflow.com/a/26240420
pids=""
for NODE in ${SLAVES}; do
  (
    echo "install ${NODE}"
    set -x
		#https://superuser.com/a/686527

    # I may need to find a way to distribute the closure
    # when it is not on nfs

    # oarcp -3 ${MASTER}:${CLOSURE} ${NODE}:${CLOSURE}
    oarsh ${NODE} "cat ${CLOSURE} |bunzip2 |nix-store \
      --import"

    oarsh ${NODE} "nix-env -iA ${ENVNAME} -f ${PKGS}"
    # --option extra-binary-caches http://${MASTER}:8080"

    set +x
  )&
  pids="$pids $!"
done

for pid in $pids; do
  wait $pid
done
