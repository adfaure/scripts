#!/bin/bash

JOB_ID=${OAR_JOB_ID}

# We get the node of the job
NODES=(`cat $HOME/.oar_tmp/${JOB_ID}`)

set -- $NODES
MASTER=$1
echo "master ${MASTER}"

# Get the list of slaves
SLAVES="${NODES[@]:1}" # removed the 1st parameter
echo "slaves:$SLAVES"

if [ ! -d "$HOME/.oar_tmp" ]; then
  mkdir -p "$HOME/.oar_tmp"
fi

# Enable history server
mkdir -p /tmp/spark-events

oarsh $MASTER "$HOME/.nix-profile/lib/spark/sbin/stop-master.sh"
oarsh $MASTER "$HOME/.nix-profile/lib/spark/sbin/stop-history-server.sh"

for SLAVE in $SLAVES; do
  # Use nohup ?
  oarsh $SLAVE "$HOME/.nix-profile/lib/spark/sbin/stop-slave.sh"
done
