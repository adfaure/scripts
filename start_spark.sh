#!/bin/bash

JOB_ID=${OAR_JOB_ID}

# We use oarprint to get the list
# of node allocated for the current job
# https://superuser.com/a/121628

NODES=( `oarprint host -P host -F '%'` )

if [ ! -d "$HOME/.oar_tmp" ]; then
  mkdir -p "$HOME/.oar_tmp"
fi

# Oarprint is not deterministic...
printf '%s\n' "${NODES[@]}" > $HOME/.oar_tmp/${JOB_ID}

# The three following lines extract
# the first element form the array
set -- $NODES
MASTER=$1

# Get the list of slaves
SLAVES="${NODES[@]:1}" # removed the 1st parameter

oarsh $MASTER "mkdir -p /tmp/spark-events"
oarsh $MASTER "$HOME/.nix-profile/lib/spark/sbin/start-master.sh"
oarsh $MASTER "$HOME/.nix-profile/lib/spark/sbin/start-history-server.sh"

for SLAVE in $SLAVES; do
  # Use nohup ?
  (set +x
  oarsh $SLAVE "mkdir -p /tmp/spark-events"
  oarsh $SLAVE "$HOME/.nix-profile/lib/spark/sbin/start-slave.sh spark://$MASTER:7077"
  set +x)&
done

echo "${MASTER}:7077" > /tmp/master-spark-afaure
