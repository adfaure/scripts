#!/bin/bash

set -eu

JOBID=${OAR_JOB_ID}

NODES=`oarprint host -P host -F '%'`

# https://stackoverflow.com/a/26240420
pids=""
for NODE in ${NODES}; do
  (
    oarsh ${NODE} sudo-g5k
    oarsh ${NODE} "sudo su root -c 'echo 1 >  \
    /proc/sys/kernel/unprivileged_userns_clone'"
    oarsh ${NODE} mkdir -p /tmp/afaure-runtime-dir
    oarsh ${NODE} "sudo curl https://nixos.org/nix/install | sh"
  )&
  pids="$pids $!"
done

for pid in $pids; do
  wait $pid
done

#source the nix on the current bash
. /home/$(whoami)/.nix-profile/etc/profile.d/nix.sh
