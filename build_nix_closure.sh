#!/bin/bash

# Create a closure from a nix-build
set -eu

OPTS=`getopt -o vhe:o:f: --long verbose,output-file:,help,nix-source:,env: -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

echo "$OPTS"
eval set -- "$OPTS"

while true; do
  case "$1" in
    -v | --verbose ) VERBOSE=true; shift ;;
    -h | --help )    HELP=true; shift ;;
    -e | --env) ENV_NAME="$2"; shift 2;;
    -o | --output-file ) OUTPUT_FILE="$2"; shift 2;;
    -f | --nix-source ) PATH_TO_DEFAULT="$2"; shift 2;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

#install the environment
nix-build $PATH_TO_DEFAULT -A $ENV_NAME -o /tmp/$ENV_NAME

# Create the closure
nix-store --export \
  $(nix-store -qR $(readlink /tmp/$ENV_NAME)) |\
  bzip2 > $OUTPUT_FILE
