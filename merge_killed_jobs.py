#!/usr/bin/env python

"""
Usage:
  merge_killed_jobs.py -f <CSV> -o <OUTPUT>

Options:
  -h --help     Show this screen.
  --version     Show version.
  -f --csv      Input job file
  -o            Output job file

"""

from docopt import docopt
import pandas as pd


def merge_job(a):
    if not isinstance(a, pd.DataFrame):
        raise()
    rej = a[a['workload_name'].str.contains("rej")]
    job = a[~a['workload_name'].str.contains("rej")]
    if rej.shape[0] == 0:
        return job


    rej.iloc[0, rej.columns.get_loc('workload_name')] = job.iloc[0, job.columns.get_loc('workload_name')]
    rej.iloc[0, rej.columns.get_loc('submission_time')] = job.iloc[0, job.columns.get_loc('submission_time')]
    rej.iloc[0, rej.columns.get_loc('waiting_time')] = rej.iloc[0, rej.columns.get_loc('starting_time')] \
        - job.iloc[0, job.columns.get_loc('submission_time')]

    rej.iloc[0, rej.columns.get_loc('stretch')] = (rej.iloc[0, rej.columns.get_loc('starting_time')] +
                    rej.iloc[0, rej.columns.get_loc('execution_time')] -\
                    job.iloc[0, job.columns.get_loc('submission_time')]) /\
                    rej.iloc[0, rej.columns.get_loc('execution_time')]

    rej.iloc[0, job.columns.get_loc('turnaround_time')] = (rej.iloc[0, rej.columns.get_loc('starting_time')] +
                    rej.iloc[0, rej.columns.get_loc('execution_time')] -\
                    job.iloc[0, job.columns.get_loc('submission_time')])
    return rej


arguments = docopt(__doc__, version='Merge rej 0.0.1')
print("Converting file: ", arguments['<CSV>'], "... to folder ",
      arguments['<OUTPUT>'])

df = pd.read_csv(arguments['<CSV>'], dtype={'workload_name': object})

result = pd.DataFrame(columns=df.columns.values)

group = df.groupby('job_id').apply(merge_job)
group.to_csv(arguments['<OUTPUT>'])
print(arguments['<CSV>'], " done.")
